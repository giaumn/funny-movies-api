class ErrorUtil
  def self.wrap(message)
    { error: message }
  end
end
