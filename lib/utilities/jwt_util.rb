class JwtUtil
  def self.encode(payload, exp = 3.days.from_now)
    payload[:exp] = exp.to_i
    payload[:iat] = Time.now.to_i
    payload[:uuid] = SecureRandom.uuid
    JWT.encode(payload, Rails.application.credentials.secret_key_base)
  end

  def self.decode(token)
    body = JWT.decode(token, Rails.application.credentials.secret_key_base, true, {})[0]
    HashWithIndifferentAccess.new body
  end
end
