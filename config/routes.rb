Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount Sidekiq::Web, at: '/sidekiq'
  namespace :v1 do
    namespace :me do
      resource :profile, only: [:show], controller: :profile
      resource :token, only: [:show], controller: :token
      resources :videos, only: [:create]
    end

    resources :tokens, only: [:create]
    resources :users, only: [:create]
    resources :videos, only: [:index] do
      member do
        resource :reaction, only: [:create], controller: :reaction
      end
    end
  end
end
