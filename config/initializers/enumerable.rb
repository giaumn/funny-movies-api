module Enumerable
  def count_by(&block)
    each_with_object(Hash.new(0)) { |element, count| count[block.call(element)] += 1 }
  end
end
