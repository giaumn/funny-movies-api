class FetchVideoInfoWorker
  include Sidekiq::Worker
  sidekiq_options retry: true

  def perform(video_id)
    video = Video.find_by(id: video_id)
    return if video.blank? || video.video_url.blank?
    video_info = VideoInfo.new(video.video_url)
    video.available = video_info.available?
    video.video_id = video_info.video_id
    video.provider = video_info.provider
    video.title = video_info.title
    video.author = video_info.author
    video.author_thumbnail = video_info.author_thumbnail
    video.author_url = video_info.author_url
    video.description = video_info.description
    video.duration = video_info.duration
    video.date = video_info.date
    video.thumbnail_small = video_info.thumbnail_small
    video.thumbnail_medium = video_info.thumbnail_medium
    video.thumbnail_large = video_info.thumbnail_large
    video.embed_url = video_info.embed_url
    video.embed_code = video_info.embed_code
    video.save!
  end
end
