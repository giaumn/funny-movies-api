class V1::VideosController < ApplicationController

  # GET /videos
  def index
    ts = request.query_parameters[:ts]
    ts = ts.blank? ? Time.now : DateTime.strptime(ts, '%Y-%m-%dT%H:%M:%S.%L%z')
    last_id = request.query_parameters[:last_id]
    videos = Video.includes([:user, :reactions]).where(available: true)
    videos = videos.where.not(title: nil)
    videos = videos.where("date_trunc('seconds', updated_at) <= ?", ts.utc.iso8601(3))
    videos = videos.where('id < ?', last_id) if last_id.present?
    videos = videos.order(updated_at: :desc, id: :desc).limit(10).to_a
    json_data = videos.as_json(
      include: {
        user: {
          except: [:password_digest]
        },
        reactions: {}
      }
    )

    json_data.each do |video|
      video['reactions_by_group'] = video['reactions'].count_by { |key| key['reaction_type'] }
      video['reaction_of_current_user'] = {}
      video['reaction_of_current_user'] = video['reactions'].find { |reaction| reaction['user_id'] == @authenticated_user.id } || {} if @authenticated_user.present?
    end

    render json: json_data
  end

  protected

  def is_public?
    true
  end
end
