class V1::UsersController < ApplicationController
  skip_before_action :authorize, only: [:create]

  # POST /users
  def create
    user_data = user_params.merge(provider: User::Provider::LOCAL, provider_id: SecureRandom.uuid)
    user_data[:password_confirmation] = '' if user_data[:password].present? && user_data[:password_confirmation].blank?
    user = User.new(user_data)
    if user.save
      user.update_attributes(provider_id: user.id)
      render json: user.as_json(except: [:password_digest]), status: :created
    else
      render json: ErrorUtil.wrap(user.errors.full_messages.first), status: :unprocessable_entity
    end
  end

  private

  # Only allow a trusted parameter "white list" through.
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
