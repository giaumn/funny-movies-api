class V1::ReactionController < ApplicationController
  before_action :set_video

  # POST /api/v1/videos/:id/reaction
  def create
    reaction = Reaction.find_by(user_id: @authenticated_user.id, video_id: @video.id)
    if reaction.blank?
      reaction = Reaction.new(reaction_params.merge(user_id: @authenticated_user.id, video_id: @video.id))
      if reaction.save
        render json: reaction, status: :created
      else
        render json: ErrorUtil.wrap(reaction.errors.full_messages.first), status: :unprocessable_entity
      end
    else
      if reaction.update(reaction_params)
        render json: reaction, status: :ok
      else
        render json: ErrorUtil.wrap(reaction.errors.full_messages.first), status: :unprocessable_entity
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_video
    @video = Video.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def reaction_params
    params.require(:reaction).permit(:reaction_type)
  end
end
