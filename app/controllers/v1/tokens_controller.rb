class V1::TokensController < ApplicationController
  skip_before_action :authorize, only: [:create]

  # POST /api/v1/tokens
  def create
    user = User.find_by(provider: params[:provider], email: params[:email])
    if user && user.authenticate(params[:password])
      token = Token.create(
        user_id: user.id,
        token_type: Token::Type::BEARER,
        token_value: JwtUtil.encode(user_id: user.id)
      )
      render json: token, status: :ok
    else
      render json: ErrorUtil.wrap('Email or password is invalid'), status: :unprocessable_entity
    end
  end
end
