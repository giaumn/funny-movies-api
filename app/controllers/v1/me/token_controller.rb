class V1::Me::TokenController < ApplicationController
  # GET /api/v1/me/token
  def show
    render json: @authenticated_token
  end
end
