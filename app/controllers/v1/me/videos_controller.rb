class V1::Me::VideosController < ApplicationController

  # POST /api/v1/me/videos
  def create
    video = Video.new(video_params.merge(user_id: @authenticated_user.id))
    if video.save
      FetchVideoInfoWorker.perform_async(video.id)
      render json: video, status: :created
    else
      render json: ErrorUtil.wrap(video.errors.full_messages.first), status: :unprocessable_entity
    end
  end

  private

  def video_params
    params.require(:video).permit(:video_url)
  end
end
