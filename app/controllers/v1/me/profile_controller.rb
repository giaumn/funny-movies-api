class V1::Me::ProfileController < ApplicationController

  # GET /api/v1/me/profile
  def show
    render json: @authenticated_user.as_json(except: [:password_digest])
  end
end
