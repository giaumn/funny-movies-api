class ApplicationController < ActionController::API
  before_action :authorize

  attr_reader :authenticated_token, :authenticated_user, :public_api

  protected

  def is_public?
    false
  end

  private

  def authorize
    if request.headers['Authorization'].present?
      access_token_info = request.headers['Authorization'].split(' ')
      if access_token_info.length != 2
        render status: :bad_request, json: ErrorUtil.wrap('Authorization header is invalid')
      else
        begin
          access_token = Token.where(token_type: access_token_info[0], token_value: access_token_info[1]).first
          if access_token.blank?
            render status: :unauthorized, json: ErrorUtil.wrap('Authorization is not found')
          else
            decoded_payload = JwtUtil.decode(access_token.token_value)
            user = User.where(id: decoded_payload[:user_id]).first
            if user.blank?
              render status: :unauthorized, json: ErrorUtil.wrap('User is not found')
            else
              @authenticated_user = user
              @authenticated_token = access_token
            end
          end
        rescue JWT::ExpiredSignature
          render status: :unauthorized, json: ErrorUtil.wrap('Session is expired')
        rescue
          render status: :unauthorized, json: ErrorUtil.wrap('Authorization is invalid')
        end
      end
    else
      render status: :bad_request, json: ErrorUtil.wrap('Authorization header is missing') unless is_public?
    end
  end

  rescue_from ActionController::ParameterMissing do |e|
    render json: { error: e.message }, status: :bad_request
  end
end
