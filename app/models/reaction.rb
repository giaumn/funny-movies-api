class Reaction < ApplicationRecord

  module Type
    LIKE = 'like'
    DISLIKE = 'dislike'
    ALL = [LIKE, DISLIKE]
  end

  belongs_to :video
  belongs_to :user

  validates :video_id, presence: true
  validates :user_id, presence: true
  validates :reaction_type, uniqueness: { scope: [:video_id, :user_id] }, inclusion: { in: Type::ALL }
end
