class Video < ApplicationRecord
  belongs_to :user
  has_many :reactions, dependent: :delete_all

  validates :user_id, presence: true
  validates :video_url, presence: true, format: { with: /(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?/ix }
  validates_uniqueness_of :video_url, message: 'has already been shared'
end
