class Token < ApplicationRecord

  module Type
    BEARER = 'Bearer'
    ALL = [BEARER]
  end

  belongs_to :user

  validates :user_id, presence: true
  validates :token_type, inclusion: { in: Type::ALL }
  validates :token_value, uniqueness: true, presence: true
end
