class User < ApplicationRecord
  has_secure_password

  module Provider
    LOCAL = 'local'
    FACEBOOK = 'facebook'
    ALL = [LOCAL, FACEBOOK]
  end

  has_many :tokens, dependent: :delete_all
  has_many :videos, dependent: :delete_all
  has_many :reactions, dependent: :delete_all

  validates :password, presence: true, length: { minimum: 6, allow_nil: true }, confirmation: true
  validates :provider, inclusion: { in: Provider::ALL }
  validates :provider_id, presence: true, uniqueness: { scope: :provider }
  validates :email, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP }, uniqueness: { scope: :provider }
end
