# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_17_062056) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "reactions", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "video_id"
    t.string "reaction_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_reactions_on_user_id"
    t.index ["video_id", "reaction_type"], name: "index_reactions_on_video_id_and_reaction_type"
    t.index ["video_id"], name: "index_reactions_on_video_id"
  end

  create_table "tokens", force: :cascade do |t|
    t.bigint "user_id"
    t.string "token_value", null: false
    t.string "token_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["token_value", "token_type"], name: "index_tokens_on_token_value_and_token_type", unique: true
    t.index ["user_id"], name: "index_tokens_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", null: false
    t.string "provider_id", null: false
    t.string "email"
    t.string "password_digest"
    t.string "first_name"
    t.string "last_name"
    t.string "avatar_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["provider", "email"], name: "index_users_on_provider_and_email", unique: true
    t.index ["provider", "provider_id"], name: "index_users_on_provider_and_provider_id", unique: true
  end

  create_table "videos", force: :cascade do |t|
    t.bigint "user_id"
    t.string "video_url", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "available"
    t.string "video_id"
    t.string "provider"
    t.string "title"
    t.string "author"
    t.string "author_thumbnail"
    t.string "author_url"
    t.text "description"
    t.datetime "date"
    t.integer "duration"
    t.string "thumbnail_small"
    t.string "thumbnail_medium"
    t.string "thumbnail_large"
    t.string "embed_url"
    t.text "embed_code"
    t.index "date_trunc('milliseconds'::text, updated_at)", name: "index_videos_on_updated_at"
    t.index ["user_id"], name: "index_videos_on_user_id"
  end

  add_foreign_key "reactions", "users"
  add_foreign_key "reactions", "videos"
  add_foreign_key "tokens", "users"
  add_foreign_key "videos", "users"
end
