class AddIndexVideosOnUpdatedAt < ActiveRecord::Migration[5.2]
  def change
    add_index :videos,
              "date_trunc('milliseconds', updated_at)",
              name: 'index_videos_on_updated_at'
  end
end
