class AddDetailFieldsToVideos < ActiveRecord::Migration[5.2]
  def change
    add_column :videos, :available, :boolean, index: true
    add_column :videos, :video_id, :string, index: true
    add_column :videos, :provider, :string, index: true
    add_column :videos, :title, :string, index: true
    add_column :videos, :author, :string
    add_column :videos, :author_thumbnail, :string
    add_column :videos, :author_url, :string
    add_column :videos, :description, :text
    add_column :videos, :date, :datetime
    add_column :videos, :duration, :integer
    add_column :videos, :thumbnail_small, :string
    add_column :videos, :thumbnail_medium, :string
    add_column :videos, :thumbnail_large, :string
    add_column :videos, :embed_url, :string
    add_column :videos, :embed_code, :text
  end
end
