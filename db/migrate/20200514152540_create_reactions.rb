class CreateReactions < ActiveRecord::Migration[5.2]
  def change
    create_table :reactions do |t|
      t.references :user, foreign_key: true, index: true
      t.references :video, foreign_key: true, index: true
      t.string :reaction_type, null: false
      t.timestamps
    end

    add_index :reactions, [:video_id, :reaction_type]
    add_index :reactions, [:user_id, :video_id, :reaction_type], unique: true
  end
end
