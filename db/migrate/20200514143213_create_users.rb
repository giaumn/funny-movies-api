class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :provider, null: false
      t.string :provider_id, null: false
      t.string :email
      t.string :password_digest
      t.string :first_name
      t.string :last_name
      t.string :avatar_path
      t.timestamps
    end

    add_index :users, [:provider, :email], unique: true
    add_index :users, [:provider, :provider_id], unique: true
  end
end
