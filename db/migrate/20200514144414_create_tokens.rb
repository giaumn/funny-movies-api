class CreateTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :tokens do |t|
      t.references :user, foreign_key: true, index: true
      t.string :token_value, null: false, unique: true
      t.string :token_type, null: false
      t.timestamps
    end

    add_index :tokens, [:token_value, :token_type], unique: true
  end
end
