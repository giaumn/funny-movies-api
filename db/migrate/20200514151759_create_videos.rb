class CreateVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :videos do |t|
      t.references :user, foreign_key: true, index: true
      t.string :video_url, null: false, unique: true
      t.timestamps
    end
  end
end
