require 'test_helper'

class V1::UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @alex = users(:alex)
    @token = tokens(:token)
  end

  test 'should create user error 400 param is not set' do
    post v1_users_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: {},
         as: :json
    assert_response :bad_request
  end

  test 'should create user error 400 param is empty' do
    post v1_users_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { user: { } },
         as: :json
    assert_response :bad_request
  end

  test 'should create user error 422 password is empty' do
    post v1_users_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { user: { password: nil } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Password can\'t be blank', error['error']
  end

  test 'should create user error 422 password confirmation not match' do
    post v1_users_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { user: { password: '12345678', password_confirmation: 'xxx' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Password confirmation doesn\'t match Password', error['error']
  end

  test 'should create user error 422 password length min 6' do
    post v1_users_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { user: { password: '12345', password_confirmation: '12345' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Password is too short (minimum is 6 characters)', error['error']
  end

  test 'should create user error 422 email is empty' do
    post v1_users_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { user: { password: '12345678', password_confirmation: '12345678', email: '' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Email can\'t be blank', error['error']
  end

  test 'should create user error 422 invalid email' do
    post v1_users_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { user: { password: '12345678', password_confirmation: '12345678', email: 'abc' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Email is invalid', error['error']
  end

  test 'should create user ok' do
    post v1_users_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { user: { password: '12345678', password_confirmation: '12345678', email: 'abc@com' } },
         as: :json
    user = JSON.parse(@response.body)
    assert_response :success
    assert_equal 'local', user['provider']
    assert_equal user['id'].to_s, user['provider_id']
    assert_equal 'abc@com', user['email']
    assert_nil user['first_name']
    assert_nil user['last_name']
    assert_nil user['avatar_path']
  end

  test 'should create user ok good email' do
    post v1_users_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { user: { password: '12345678', password_confirmation: '12345678', email: 'alex.henderson@test.com' } },
         as: :json
    user = JSON.parse(@response.body)
    assert_response :success
    assert_equal 'local', user['provider']
    assert_equal user['id'].to_s, user['provider_id']
    assert_equal 'alex.henderson@test.com', user['email']
    assert_nil user['first_name']
    assert_nil user['last_name']
    assert_nil user['avatar_path']
  end

  test 'should create user error duplicate email' do
    post v1_users_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { user: { password: '12345678', password_confirmation: '12345678', email: 'alex@test.com' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Email has already been taken', error['error']
  end
end
