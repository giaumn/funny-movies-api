require 'test_helper'

class V1::Me::ProfileControllerTest < ActionDispatch::IntegrationTest
  setup do
    @alex = users(:alex)
    @token = tokens(:token)
  end

  test 'should get user profile' do
    get v1_me_profile_path, headers: { Authorization: "#{@token.token_type} #{@token.token_value}"}, as: :json
    user = JSON.parse(@response.body)
    assert_response :success
    assert_equal 'local', user['provider']
    assert_equal '1', user['provider_id']
    assert_equal 'alex@test.com', user['email']
    assert_equal 'Alex', user['first_name']
    assert_equal 'Handerson', user['last_name']
    assert_equal '/users/1/avatar.png', user['avatar_path']
  end
end
