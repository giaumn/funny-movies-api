require 'test_helper'

class V1::Me::TokenControllerTest < ActionDispatch::IntegrationTest
  setup do
    @alex = users(:alex)
    @token = tokens(:token)
  end

  test 'should get token' do
    get v1_me_token_path, headers: { Authorization: "#{@token.token_type} #{@token.token_value}"}, as: :json
    token = JSON.parse(@response.body)
    assert_response :success
    assert_equal @token.token_value, token['token_value']
    assert_equal 'Bearer', token['token_type']
    assert_equal @alex.id, token['user_id']
  end
end
