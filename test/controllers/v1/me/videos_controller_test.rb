require 'test_helper'

class V1::Me::VideosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @alex = users(:alex)
    @token = tokens(:token)
  end

  test 'should create video error 400 param is not set' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: {},
         as: :json
    assert_response :bad_request
  end

  test 'should create video error 400 video_url is not set' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { } },
         as: :json
    assert_response :bad_request
  end

  test 'should create video error 422 video_url is empty string' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: '' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Video url can\'t be blank', error['error']
  end

  test 'should create video error 422 video_url is null' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: nil } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Video url can\'t be blank', error['error']
  end

  test 'should create video error 422 invalid url' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: 'xx' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Video url is invalid', error['error']
  end

  test 'should create video error 422 http invalid url' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: 'http:' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Video url is invalid', error['error']
  end

  test 'should create video error 422 https invalid url' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: 'https:' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Video url is invalid', error['error']
  end

  test 'should create video error 422 http:// invalid url' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: 'http://' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Video url is invalid', error['error']
  end

  test 'should create video error 422 http://abc invalid url' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: 'http://abc' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Video url is invalid', error['error']
  end

  test 'should create video error 422 http://abc. invalid url' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: 'http://abc.' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Video url is invalid', error['error']
  end

  test 'should create video ok http://abc.co invalid url' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: 'http://abc.co' } },
         as: :json
    video = JSON.parse(@response.body)
    assert_response :success
    assert_equal @alex.id, video['user_id']
    assert_equal 'http://abc.co', video['video_url']
  end

  test 'should create video ok with youtube full url' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: 'https://www.youtube.com/watch?v=0YxPKX8hZbQ' } },
         as: :json
    video = JSON.parse(@response.body)
    assert_response :success
    assert_equal @alex.id, video['user_id']
    assert_equal 'https://www.youtube.com/watch?v=0YxPKX8hZbQ', video['video_url']
  end

  test 'should create video error duplicate youtube url' do
    post v1_me_videos_path,
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { video: { video_url: 'https://www.youtube.com/watch?v=CQ-5Ih_OD4I' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Video url has already been shared', error['error']
  end
end
