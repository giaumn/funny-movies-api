require 'test_helper'

class V1::ReactionControllerTest < ActionDispatch::IntegrationTest
  setup do
    @alex = users(:alex)
    @token = tokens(:token)
    @video1 = videos(:video1)
    @video2 = videos(:video2)
  end

  test 'should create reaction error 400 param is not set' do
    post v1_reaction_path(@video1.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: {},
         as: :json
    assert_response :bad_request
  end

  test 'should create reaction error 400 param is empty' do
    post v1_reaction_path(@video1.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { reaction: { } },
         as: :json
    assert_response :bad_request
  end

  test 'should create reaction error 422 reaction_type is empty' do
    post v1_reaction_path(@video1.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { reaction: { reaction_type: nil } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Reaction type is not included in the list', error['error']
  end

  test 'should create reaction error 422 reaction_type is invalid' do
    post v1_reaction_path(@video1.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { reaction: { reaction_type: 'invalid-type' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Reaction type is not included in the list', error['error']
  end

  test 'should create reaction like ok' do
    post v1_reaction_path(@video1.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { reaction: { reaction_type: 'like' } },
         as: :json
    reaction = JSON.parse(@response.body)
    assert_response :success
    assert_equal @alex.id, reaction['user_id']
    assert_equal @video1.id, reaction['video_id']
    assert_equal 'like', reaction['reaction_type']
  end

  test 'should update reaction to dislike ok' do
    post v1_reaction_path(@video1.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { reaction: { reaction_type: 'like' } },
         as: :json
    reaction = JSON.parse(@response.body)
    assert_response :success
    assert_equal @alex.id, reaction['user_id']
    assert_equal @video1.id, reaction['video_id']
    assert_equal 'like', reaction['reaction_type']

    post v1_reaction_path(@video1.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { reaction: { reaction_type: 'dislike' } },
         as: :json
    reaction = JSON.parse(@response.body)
    assert_response :success
    assert_equal @alex.id, reaction['user_id']
    assert_equal @video1.id, reaction['video_id']
    assert_equal 'dislike', reaction['reaction_type']
  end

  test 'should update reaction invalid type error' do
    post v1_reaction_path(@video1.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { reaction: { reaction_type: 'like' } },
         as: :json
    reaction = JSON.parse(@response.body)
    assert_response :success
    assert_equal @alex.id, reaction['user_id']
    assert_equal @video1.id, reaction['video_id']
    assert_equal 'like', reaction['reaction_type']

    post v1_reaction_path(@video1.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { reaction: { reaction_type: 'invalid-type' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Reaction type is not included in the list', error['error']
  end

  test 'should update reaction error 422 reaction_type is invalid' do
    post v1_reaction_path(@video2.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { reaction: { reaction_type: 'invalid-type' } },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Reaction type is not included in the list', error['error']
  end

  test 'should create reaction dislike video ok' do
    post v1_reaction_path(@video2.id),
         headers: { Authorization: "#{@token.token_type} #{@token.token_value}" },
         params: { reaction: { reaction_type: 'dislike' } },
         as: :json
    reaction = JSON.parse(@response.body)
    assert_response :success
    assert_equal @alex.id, reaction['user_id']
    assert_equal @video2.id, reaction['video_id']
    assert_equal 'dislike', reaction['reaction_type']
  end
end
