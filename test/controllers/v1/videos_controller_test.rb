require 'test_helper'

class V1::VideosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @alex = users(:alex)
    @token = tokens(:token)
    @video1 = videos(:video1)
  end

  test 'should get top 10 videos' do
    get v1_videos_path, headers: { }, as: :json
    response = JSON.parse(@response.body)
    assert_response :success
    assert_equal 10, response.length
    assert_equal 28, response[0]['id']
    assert_equal 27, response[1]['id']
    assert_equal 26, response[2]['id']
    assert_equal 25, response[3]['id']
    assert_equal 24, response[4]['id']
    assert_equal 23, response[5]['id']
    assert_equal 22, response[6]['id']
    assert_equal 21, response[7]['id']
    assert_equal 20, response[8]['id']
    assert_equal 19, response[9]['id']
  end

  test 'should get next 10 videos' do
    get v1_videos_path({ts: Time.parse(@video1.updated_at.to_s).iso8601(3), last_id: 19}), headers: { Authorization: "#{@token.token_type} #{@token.token_value}"}, as: :json
    response = JSON.parse(@response.body)
    assert_response :success
    assert_equal 10, response.length
    assert_equal 18, response[0]['id']
    assert_equal 17, response[1]['id']
    assert_equal 16, response[2]['id']
    assert_equal 15, response[3]['id']
    assert_equal 14, response[4]['id']
    assert_equal 13, response[5]['id']
    assert_equal 12, response[6]['id']
    assert_equal 11, response[7]['id']
    assert_equal 10, response[8]['id']
    assert_equal 9, response[9]['id']
  end

  test 'should get last 10 videos' do
    get v1_videos_path({ts: Time.parse(@video1.updated_at.to_s).iso8601(3), last_id: 9}), headers: { Authorization: "#{@token.token_type} #{@token.token_value}"}, as: :json
    response = JSON.parse(@response.body)
    assert_response :success
    assert_equal 8, response.length
    assert_equal 8, response[0]['id']
    assert_equal 7, response[1]['id']
    assert_equal 6, response[2]['id']
    assert_equal 5, response[3]['id']
    assert_equal 4, response[4]['id']
    assert_equal 3, response[5]['id']
    assert_equal 2, response[6]['id']
    assert_equal 1, response[7]['id']
  end
end
