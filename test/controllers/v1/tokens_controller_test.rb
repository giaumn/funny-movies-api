require 'test_helper'

class V1::TokensControllerTest < ActionDispatch::IntegrationTest
  setup do
    @alex = users(:alex)
    @token = tokens(:token)
  end

  test 'should create token error 422 provider is empty' do
    post v1_tokens_path,
         headers: { },
         params: { },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Email or password is invalid', error['error']
  end

  test 'should create token error 422 provider is invalid' do
    post v1_tokens_path,
         headers: { },
         params: { provider: 'xxx' },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Email or password is invalid', error['error']
  end

  test 'should create token error 422 email is empty' do
    post v1_tokens_path,
         headers: { },
         params: { provider: 'local' },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Email or password is invalid', error['error']
  end

  test 'should create token error 422 email is invalid' do
    post v1_tokens_path,
         headers: { },
         params: { provider: 'local', email: 'fakeemail@test.com' },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Email or password is invalid', error['error']
  end

  test 'should create token error 422 password is empty' do
    post v1_tokens_path,
         headers: { },
         params: { provider: 'local', email: 'alex@test.com' },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Email or password is invalid', error['error']
  end

  test 'should create token error 422 password is wrong' do
    post v1_tokens_path,
         headers: { },
         params: { provider: 'local', email: 'alex@test.com', password: '123X222ad@' },
         as: :json
    error = JSON.parse(@response.body)
    assert_response :unprocessable_entity
    assert_equal 'Email or password is invalid', error['error']
  end

  test 'should create token success' do
    post v1_tokens_path,
         headers: { },
         params: { provider: 'local', email: 'alex@test.com', password: '12345678' },
         as: :json
    token = JSON.parse(@response.body)
    assert_response :success
    assert_not_nil token['token_value']
    assert_equal 'Bearer', token['token_type']
    assert_equal @alex.id, token['user_id']
  end
end
