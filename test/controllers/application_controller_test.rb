require 'test_helper'

class ApplicationControllerTest < ActionDispatch::IntegrationTest
  setup do
    @alex = users(:alex)
    @token = tokens(:token)
    @user_not_found_token = tokens(:user_not_found_token)
    @expired_token = tokens(:expired_token)
  end

  test 'should authorize error header missing' do
    get v1_me_profile_path, headers: {}, as: :json
    error = JSON.parse(@response.body)
    assert_response :bad_request
    assert_equal 'Authorization header is missing', error['error']
  end

  test 'should authorize error header invalid' do
    get v1_me_profile_path, headers: { Authorization: 'xx' }, as: :json
    error = JSON.parse(@response.body)
    assert_response :bad_request
    assert_equal 'Authorization header is invalid', error['error']
  end

  test 'should authorize error header invalid 1 part' do
    get v1_me_profile_path, headers: { Authorization: 'xx ' }, as: :json
    error = JSON.parse(@response.body)
    assert_response :bad_request
    assert_equal 'Authorization header is invalid', error['error']
  end

  test 'should authorize error header invalid 3 part' do
    get v1_me_profile_path, headers: { Authorization: 'xx y z' }, as: :json
    error = JSON.parse(@response.body)
    assert_response :bad_request
    assert_equal 'Authorization header is invalid', error['error']
  end

  test 'should authorize error token not found' do
    get v1_me_profile_path, headers: { Authorization: 'xx y' }, as: :json
    error = JSON.parse(@response.body)
    assert_response :unauthorized
    assert_equal 'Authorization is not found', error['error']
  end

  test 'should authorize error token found but decode error' do
    get v1_me_profile_path, headers: { Authorization: 'Bearer xxxyyyzzz' }, as: :json
    error = JSON.parse(@response.body)
    assert_response :unauthorized
    assert_equal 'Authorization is invalid', error['error']
  end

  test 'should authorize error token found but user is not found' do
    get v1_me_profile_path, headers: { Authorization: "#{@user_not_found_token.token_type} #{@user_not_found_token.token_value}" }, as: :json
    error = JSON.parse(@response.body)
    assert_response :unauthorized
    assert_equal 'User is not found', error['error']
  end

  test 'should authorize error token found but expired' do
    get v1_me_profile_path, headers: { Authorization: "#{@expired_token.token_type} #{@expired_token.token_value}" }, as: :json
    error = JSON.parse(@response.body)
    assert_response :unauthorized
    assert_equal 'Session is expired', error['error']
  end
end
