require 'test_helper'

class FetchVideoInfoWorkerTest < ActiveSupport::TestCase
  setup do
    @alex = users(:alex)
    @unavailable_video1 = videos(:unavailable_video1)
    @invalid_youtube_url_video1 = videos(:invalid_youtube_url_video1)
  end

  test 'should fetch video ok' do
    FetchVideoInfoWorker.new.perform(@unavailable_video1.id)
    @unavailable_video1.reload
    assert @unavailable_video1.available
    assert_equal 'YouTube', @unavailable_video1.provider
    assert_equal 'BEST CUBE #1 BEST COUB 2020 May', @unavailable_video1.title
    assert_equal 'Best Cube', @unavailable_video1.author
  end

  test 'should fetch video error' do
    exception = assert_raises(VideoInfo::UrlError) { FetchVideoInfoWorker.new.perform(@invalid_youtube_url_video1.id) }
    @invalid_youtube_url_video1.reload
    assert_not @invalid_youtube_url_video1.available
    assert_nil @invalid_youtube_url_video1.provider
    assert_nil @invalid_youtube_url_video1.title
    assert_nil @invalid_youtube_url_video1.author
    assert_equal 'Url is not valid, video_id is not found: https://www.youtube.com/xxxaw2sadsdadas', exception.message
  end
end
